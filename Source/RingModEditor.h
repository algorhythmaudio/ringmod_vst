//
//
// File:    RingModEditor.h
// Project: The Ring VST plug-in
//
// Created: June 3, 2012 by Christian Floisand
//
// Copyright (c) 2012, AlgoRhythm Audio. All rights reserved.
//
//

#ifndef _RingModEditor_H_
#define _RingModEditor_H_

#include "vstgui4/vstgui/plugin-bindings/aeffguieditor.h"
#include "RingMod.h"
#include "AAVSTKnob.h"
#include <fstream>
	
#pragma mark ____RingModEditor Declaration
//---------------------------------------------------------------------------------------------
class RingModEditor : public AEffGUIEditor, public CControlListener {

public:
	RingModEditor (void*);
	
	bool open (void* ptr);
	void close ();
	void setParameter (VstInt32 index, float value);
	
	void valueChanged (CControl* pControl);
	void beginEdit (int32_t index);
	void endEdit (int32_t index);
    
    // plug-in calls this to notify editor that a parameter change has been set by a program
    void notifyEditorOfProgramSet ()
    {
        plugProgramSet = true;
    }
	
private:
	
	// gui control tags
	enum {
		guiControl_Knob_Freq,
		guiControl_Knob_Depth,
		guiControl_Button_Sine,
		guiControl_Button_SawUp,
		guiControl_Button_SawDn,
		guiControl_Button_Tri,
		guiControl_Button_Square,
		guiControl_Text_Freq,
		guiControl_Text_Depth,
		guiControl_NControls
	};
	
	CControl	*control[guiControl_NControls-2];	// controls for the two knobs (freq & depth) and the five buttons for wave type
    AAVSTKnob	*knob[aAParam_NParams-1];           // the two knobs, for frequency and depth
	CTextEdit	*label[aAParam_NParams-1];			// text labels displaying the values for the two knobs, editable by user
	CMovieButton *buttonWave[aAWave_NTypes];		// buttons for the five wave types
	ushort      lastWaveButton;                     // stores last wave button that was "on" so we can turn it "off" when
                                                    // another button is pressed
    bool		plugProgramSet;						// makes sure controls that need to update their delay element due to filtering does so
};


#endif // _RingModEditor_H_