//
//
// File:    RingModEditor.cpp
// Project: The Ring VST plug-in
//
// Plug-in editor definitions.
//
//

#include "RingModEditor.h"


// functions for converting the text->value & value->text in the CTextEdit param labels
//--------------------------------------------------------------------------------------------
bool freqStringToVal (UTF8StringPtr text, float &result, void *data)
{
	// convert label's string to float, and normalize to VST param range
	result = freq2float(atof(text));
	return true;
}

bool freqValueToStr (float val, char str[256], void *data)
{
	float valAsHz = float2freq(val);
	VstInt32 strLen = (valAsHz < 10. ? 4 : (valAsHz < 100. ? 5 : (valAsHz < 1000. ? 6 : 7)));
	
	// convert label's value to string, and convert to user frequency display range
	// (force to maximum 6 digit precision (include "." in string length))
	((AudioEffectX*)data)->float2string(float2freq(val), str, strLen);
	return true;
}

bool depthStringToVal (UTF8StringPtr text, float &result, void *data)
{
	result = depth2float(atof(text));
	return true;
}

bool depthValueToStr (float val, char str[256], void *data)
{
	float valAsPerc = float2depth(val);
	VstInt32 strLen = (valAsPerc < 10. ? 4 : (valAsPerc < 100. ? 5 : 6));
	
	// convert label's value to string, and convert to user frequency display range
	// (force to maximum 6 digit precision (include "." in string length))
	((AudioEffectX*)data)->float2string(float2depth(val), str, strLen);
	return true;
}
//--------------------------------------------------------------------------------------------


#pragma mark ____VSTGUI Definitions
//--------------------------------------------------------------------------------------------
AEffGUIEditor* createEditor (AudioEffectX* effect)
{
	return new RingModEditor (effect);
}
//--------------------------------------------------------------------------------------------
RingModEditor::RingModEditor (void* ptr) : AEffGUIEditor (ptr)
{}
//--------------------------------------------------------------------------------------------
bool RingModEditor::open (void* ptr)
{	
	// get the rectangle of the window the host opens, and set it's dimensions
	ERect *wSize;
	getRect(&wSize);
	wSize->top = wSize->left = 0;
	wSize->bottom = editorWindowHeight;
	wSize->right = editorWindowWidth;
	
	// the main viewing frame
	CRect frameSize (0, 0, editorWindowWidth, editorWindowHeight);
	CFrame* newFrame = new CFrame (frameSize, ptr, this);
	CBitmap* backgroundBitmap = new CBitmap ("RMBackground.png");
	newFrame->setBackground(backgroundBitmap);
	
	CBitmap* knobBitmap = new CBitmap ("RMKnob.png");
	CBitmap* sineBitmap = new CBitmap ("RMSine.png");
	CBitmap* sawUpBitmap = new CBitmap ("RMSawUp.png");
	CBitmap* sawDownBitmap = new CBitmap ("RMSawDn.png");
	CBitmap* triBitmap = new CBitmap ("RMTri.png");
	CBitmap* squareBitmap = new CBitmap ("RMSquare.png");

	// create knobs for frequency and gain and adding them to the frame
	CRect knobRect (0, 0, knobBitmap->getWidth (), knobBitmap->getHeight ());
	knobRect.offset (88, 132);
	knob[aAParam_Freq] = new AAVSTKnob (knobRect, this, guiControl_Knob_Freq, knobBitmap, NULL);
	knobRect.offset (knobBitmap->getWidth () + 114, 0);
	knob[aAParam_Depth] = new AAVSTKnob (knobRect, this, guiControl_Knob_Depth, knobBitmap, NULL);
	CColor knobHandleColor(69, 156, 69, 255);		// green
	CColor knobHandleShadowColor(38, 42, 87, 255);	// dark blue
	
	// frequency parameter knob
	knob[aAParam_Freq]->setHandleLineWidth(2);
	knob[aAParam_Freq]->setColorHandle(knobHandleColor);
	knob[aAParam_Freq]->setColorShadowHandle(knobHandleShadowColor);
    knob[aAParam_Freq]->setZoomFactor(4.5);
    knob[aAParam_Freq]->setWheelInc(0.005);
    knob[aAParam_Freq]->setDefaultValue(freq2float(factoryPresets_paramFreq[0]));
    knob[aAParam_Freq]->setCurvature(2.2);
    knob[aAParam_Freq]->setSmoothing(0.055, knob[aAParam_Freq]->getDefaultValue());
	newFrame->addView (knob[aAParam_Freq]);
	
	// depth parameter knob
	knob[aAParam_Depth]->setHandleLineWidth(2);
	knob[aAParam_Depth]->setColorHandle(knobHandleColor);
	knob[aAParam_Depth]->setColorShadowHandle(knobHandleShadowColor);
    knob[aAParam_Depth]->setZoomFactor(4.5);
    knob[aAParam_Depth]->setWheelInc(0.005);
    knob[aAParam_Depth]->setDefaultValue(depth2float(factoryPresets_paramDepth[0]));
    knob[aAParam_Depth]->setCurvature(1.);	// 1 = no curvature
    knob[aAParam_Depth]->setSmoothing(0.055, knob[aAParam_Depth]->getDefaultValue());
	newFrame->addView (knob[aAParam_Depth]);
	
	// set parameter display labels
	CRect displayLabel (0, 0, 54, 18); displayLabel.offset (73, 110);
	label[aAParam_Freq] = new CTextEdit (displayLabel, this, guiControl_Text_Freq);
	displayLabel.offset (knobBitmap->getWidth() + 116, 0);
	label[aAParam_Depth] = new CTextEdit (displayLabel, this, guiControl_Text_Depth);
	CColor labelTransparent(0, 0, 0, 0);	// black = alpha results in transparent
	CColor labelFontColor(255, 255, 255);	// white
	
	// frequency parameter text display, editable
	label[aAParam_Freq]->setStringToValueProc(&freqStringToVal, NULL);
	label[aAParam_Freq]->setValueToStringProc(&freqValueToStr, effect);
	label[aAParam_Freq]->setMin(freq2float(freqLowerLimit));
	label[aAParam_Freq]->setMax(freq2float(freqLowerLimit+freqUpperLimit));
	label[aAParam_Freq]->setValue(effect->getParameter(aAParam_Freq));
	label[aAParam_Freq]->setBackColor(labelTransparent);
	label[aAParam_Freq]->setFrameColor(labelTransparent);
	label[aAParam_Freq]->setFontColor(labelFontColor);
	newFrame->addView(label[aAParam_Freq]);
	
	// depth parameter text display, editable
	label[aAParam_Depth]->setStringToValueProc(&depthStringToVal, NULL);
	label[aAParam_Depth]->setValueToStringProc(&depthValueToStr, effect);
	label[aAParam_Depth]->setMin(0.);
	label[aAParam_Depth]->setMax(100.);
	label[aAParam_Depth]->setValue(effect->getParameter(aAParam_Depth));
	label[aAParam_Depth]->setBackColor(labelTransparent);
	label[aAParam_Depth]->setFrameColor(labelTransparent);
	label[aAParam_Depth]->setFontColor(labelFontColor);
	newFrame->addView(label[aAParam_Depth]);
	
	// set up buttons for wavetype parameter
	// offset the button tag IDs by 2 because aAwave enums start at 0, and account for Freq & Depth params
	//	getHeight()/2 because 2 bitmaps are stacked in source images
	CRect buttonRect (0, 0, sineBitmap->getWidth (), sineBitmap->getHeight ()/2);
	
	buttonRect.offset (10, 74);
	buttonWave[aAWave_Sine] = new CMovieButton (buttonRect, this, guiControl_Button_Sine, 
												  sineBitmap->getHeight()/2, sineBitmap);
	buttonWave[aAWave_Sine]->setNumSubPixmaps(1);
	newFrame->addView(buttonWave[aAWave_Sine]);
	
	buttonRect.offset (sawUpBitmap->getWidth() + 10, 0);
	buttonWave[aAWave_SawUp] = new CMovieButton (buttonRect, this, guiControl_Button_SawUp, 
												   sawUpBitmap->getHeight()/2, sawUpBitmap);
	buttonWave[aAWave_SawUp]->setNumSubPixmaps(1);
	newFrame->addView(buttonWave[aAWave_SawUp]);
	
	buttonRect.offset (sawDownBitmap->getWidth() + 10, 0);
	buttonWave[aAWave_SawDn] = new CMovieButton (buttonRect, this, guiControl_Button_SawDn, 
												   sawDownBitmap->getHeight()/2, sawDownBitmap);
	buttonWave[aAWave_SawDn]->setNumSubPixmaps(1);
	newFrame->addView(buttonWave[aAWave_SawDn]);
	
	buttonRect.offset (triBitmap->getWidth() + 10, 0);
	buttonWave[aAWave_Tri] = new CMovieButton (buttonRect, this, guiControl_Button_Tri, 
												 triBitmap->getHeight()/2, triBitmap);
	buttonWave[aAWave_Tri]->setNumSubPixmaps(1);
	newFrame->addView(buttonWave[aAWave_Tri]);
	
	buttonRect.offset (squareBitmap->getWidth() + 10, 0);
	buttonWave[aAWave_Square] = new CMovieButton (buttonRect, this, guiControl_Button_Square, 
													squareBitmap->getHeight()/2, squareBitmap);
	buttonWave[aAWave_Square]->setNumSubPixmaps(1);
	newFrame->addView(buttonWave[aAWave_Square]);
	
	backgroundBitmap->forget();
	knobBitmap->forget();
	sineBitmap->forget();
	sawUpBitmap->forget();
	sawDownBitmap->forget();
	triBitmap->forget();
	squareBitmap->forget();
	
	// assign defined parameter controls to control listener
	control[guiControl_Knob_Freq] = knob[aAParam_Freq];
	control[guiControl_Knob_Depth] = knob[aAParam_Depth];
	control[guiControl_Button_Sine] = buttonWave[aAWave_Sine];
	control[guiControl_Button_SawUp] = buttonWave[aAWave_SawUp];
	control[guiControl_Button_SawDn] = buttonWave[aAWave_SawDn];
	control[guiControl_Button_Tri] = buttonWave[aAWave_Tri];
	control[guiControl_Button_Square] = buttonWave[aAWave_Square];
	
	// arbitrarily set lastWaveButton so we don't crash due to out-of-bounds indexing first time we call setParameter
	lastWaveButton = aAWave_Square;
    
    plugProgramSet = false;
	frame = newFrame;
	setKnobMode(kLinearMode);
	
	//edout.open("/Users/Rainland/Desktop/editor.txt");
	
	// get parameter values from effect and set their value to the GUI controls
	for (int i = 0; i < aAParam_NParams; i++)
		setParameter(i, effect->getParameter(i));
	
	return true;
}
//--------------------------------------------------------------------------------------------
void RingModEditor::close ()
{    
	CFrame* oldFrame = frame;
	frame = 0;
	oldFrame->forget();
}
//--------------------------------------------------------------------------------------------
void RingModEditor::valueChanged (CControl* pControl)
{
	float paramValue = 0.f;
	VstInt32 paramTag = 0;
	
	// scale parameter to correct display value for user
	switch (pControl->getTag()) {
			
		case guiControl_Knob_Freq:
			paramTag = aAParam_Freq;
			paramValue = pControl->getValue();
			break;
			
		case guiControl_Knob_Depth:
			paramTag = aAParam_Depth;
			paramValue = pControl->getValue();
			break;
			
		// handle wave parameter mapping (each button maps to aAParam_Wavetype to send to effect)
		case guiControl_Button_Sine:
			paramTag = aAParam_Wavetype;
			paramValue = wavetype2float(aAWave_Sine);
			break;
			
		case guiControl_Button_SawUp:
			paramTag = aAParam_Wavetype;
			paramValue = wavetype2float(aAWave_SawUp);
			break;
			
		case guiControl_Button_SawDn:
			paramTag = aAParam_Wavetype;
			paramValue = wavetype2float(aAWave_SawDn);
			break;
			
		case guiControl_Button_Tri:
			paramTag = aAParam_Wavetype;
			paramValue = wavetype2float(aAWave_Tri);
			break;
			
		case guiControl_Button_Square:
			paramTag = aAParam_Wavetype;
			paramValue = wavetype2float(aAWave_Square);
			break;
			
		case guiControl_Text_Freq:
			paramTag = aAParam_Freq;
			paramValue = label[aAParam_Freq]->getValue();
			
			// make sure string value in text label also obeys parameter bounds
			if (float2freq(paramValue) >= (freqLowerLimit+freqUpperLimit))
				label[aAParam_Freq]->setText(freq_UpperLimit_Str);
			if (float2freq(paramValue) <= freqLowerLimit)
				label[aAParam_Freq]->setText(freq_LowerLimit_Str);
			
            // update smoothing filter's delay element and make sure knob is redrawn
            ((AAVSTKnob*)control[guiControl_Knob_Freq])->updateDelayElement(paramValue);
			control[guiControl_Knob_Freq]->invalid();
			break;
			
		case guiControl_Text_Depth:
			paramTag = aAParam_Depth;
			paramValue = label[aAParam_Depth]->getValue();
			
			// make sure string value in text label also obeys parameter bounds
			if (float2depth(paramValue) >= 100.f)
				label[aAParam_Depth]->setText(depth_UpperLimit_Str);
			if (float2depth(paramValue) <= 0.f)
				label[aAParam_Depth]->setText(depth_LowerLimit_Str);
			
            // update smoothing filter's delay element and make sure knob is redrawn
            ((AAVSTKnob*)control[guiControl_Knob_Depth])->updateDelayElement(paramValue);
			control[guiControl_Knob_Depth]->invalid();
	}
	
	// send parameter change to plug-in
	//((AudioEffectX*)effect)->beginEdit(paramTag);
	effect->setParameterAutomated(paramTag, paramValue);
}
//--------------------------------------------------------------------------------------------
void RingModEditor::beginEdit (int32_t index)
{
	if (index > guiControl_Knob_Depth)
		index = aAParam_Wavetype;
	AEffGUIEditor::beginEdit(index);
}
//--------------------------------------------------------------------------------------------
void RingModEditor::endEdit (int32_t index)
{
	if (index > guiControl_Knob_Depth)
		index = aAParam_Wavetype;
	AEffGUIEditor::endEdit(index);
}
//--------------------------------------------------------------------------------------------
void RingModEditor::setParameter (VstInt32 index, float value)
{
	if (frame && index < aAParam_NParams) {
		
		// handle wave parameter mapping
		// since each CMovieButton representing wave types has a unique tag, we need to map the value coming in from
		// the effect to proper index for wavesButtons[] and turn off the last button that was on
		if (index == aAParam_Wavetype) {
			control[float2wavetype(value)+2]->setValue(1);
			
			if (lastWaveButton != float2wavetype(value))
				control[lastWaveButton+2]->setValue(0);
			
			lastWaveButton = float2wavetype(value);
		}
		else {	// update controls and labels of freq/depth params
			control[index]->setValue(value);
            label[index]->setValue(value);
            
            if (plugProgramSet) {
                ((AAVSTKnob*)control[index])->updateDelayElement(value);
                plugProgramSet = false;
            }
		}
	}
}


