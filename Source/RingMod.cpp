//
//
// File:    RingMod.cpp
// Project: The Ring VST plug-in
//
// Plug-in definitions.
//
//

#include "RingMod.h"
#include "RingModEditor.h"


using namespace AADsp;


#pragma mark ____Plug initialization
//--------------------------------------------------------------------------------------------
AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
{
	RingModPlug* effect = new RingModPlug(audioMaster);
	if (!effect)
		return NULL;
	return effect;
}
//--------------------------------------------------------------------------------------------
RingModPlug::RingModPlug (audioMasterCallback audioMaster) : AudioEffectX(audioMaster, aAProg_NPrograms, aAParam_NParams),
	sampleRate(getSampleRate()),
	paramFreq(freq2float(factoryPresets_paramFreq[0])),
	paramDepth(depth2float(factoryPresets_paramDepth[0])),
	paramWavetype(wavetype2float(factoryPresets_paramWavetype[0])),
	lastFreq(paramFreq),
	lastWave(paramWavetype),
	modulator(wavetableSizeInBits, sampleRate*upsampleFactor),
	maxBlockSize(0)
{
#ifdef DEBUG
	
    openDebugFile();
	DEBUGLOG("The Ring VST Plug-in Debug Mode, version " << RingModVersionStr << '\n');
	
	char hostVendorString[kVstMaxVendorStrLen] = "";
	char hostProductString[kVstMaxProductStrLen] = "";
	if (getHostVendorString(hostVendorString))		DEBUGLOG("Host vendor: " << hostVendorString << '\n');
	if (getHostProductString(hostProductString))	DEBUGLOG("Host product: " << hostProductString << '\n');
	if (getHostVendorVersion() != 0)				DEBUGLOG("Version: " << getHostVendorVersion() << '\n');
    DEBUGLOG('\n');
    DEBUGLOG("Upsampling factor set at " << upsampleFactor << '\n');
	DEBUGLOG("Wave table size set to " << (1<<wavetableSizeInBits) << '\n');
	DEBUGLOG("Current process level: " << getCurrentProcessLevel() << '\n');
	DEBUGLOG("Current automation state: " << getAutomationState() << '\n' << '\n');
	
#endif	// DEBUG
	
	// set editor for plug-in GUI
	extern AEffGUIEditor* createEditor (AudioEffectX*);
	setEditor (createEditor (this));
	
	// initialize programs/factory presets, and parameters to startup defaults
	plugPrograms = new RingModProgram[aAProg_NPrograms];
	initFactoryPresets();
	setProgram(aAProg_Default);
	
	// create all wavetables
	modulator.initOsc(NULL, 0, 6, 6, 10, 6);	// harmonics: 6 for sawtooth up/down, 10 for triangle, 6 for square
    modulator.setType((OscType)float2wavetype(paramWavetype));
	modulator.setFreq(float2freq(paramFreq));
    modulator.setAmp(paramDepth);
    
    for (int i = 0; i < 2; ++i) {
        interpolator[i].initialize(sampleRate, upsampleFactor, resampleQuality_Good);
        decimator[i].initialize(sampleRate*upsampleFactor, upsampleFactor, resampleQuality_Good);
    }
	
	// set input/output to STEREO, and uniqueID
	setNumInputs(2);
	setNumOutputs(2);
	setUniqueID(CCONST('A', 'l', 'R', 'M'));
	canProcessReplacing();
    noTail(true);
    
	resume();
}
//--------------------------------------------------------------------------------------------
void RingModPlug::resume ()
{
	if (sampleRate != updateSampleRate()) {
		sampleRate = updateSampleRate();
		modulator.setSampleRate(sampleRate*upsampleFactor);
        for (int i = 0; i < 2; ++i) {
			interpolator[i].initialize(sampleRate, upsampleFactor, resampleQuality_Good);
			decimator[i].initialize(sampleRate*upsampleFactor, upsampleFactor, resampleQuality_Good);
		}
	}
    
    // allocate enough memory in tempBuffer to hold the maximum block size possible from host
    if (maxBlockSize != getBlockSize()) {
        maxBlockSize = getBlockSize();
		uint totalBufferLength = maxBlockSize * upsampleFactor;
		
		modulator.initSigVector(totalBufferLength);
		modulatorSignal.resize_clear(totalBufferLength);
		
        tempBuffer[0].resize_clear(totalBufferLength);
		tempBuffer[1].resize_clear(totalBufferLength);
    }
    
    for (int i = 0; i < 2; ++i) {
        interpolator[i].reset();
        decimator[i].reset();
    }
	
	modulator.setPhase(0);
	
#ifdef DEBUG
	
	DEBUGLOG("resume()--\n\tfreq = " << float2freq(paramFreq) << ", depth = " << float2depth(paramDepth) <<
             ", wavetype = " << float2wavetype(paramWavetype) << ", current sample rate: " << sampleRate << '\n' << 
             "\tmaximum block size: " << maxBlockSize << ", getInputLatency(): " << getInputLatency() <<
             ", getOutputLatency(): " << getOutputLatency() << '\n');
    DEBUGLOG("AAOsc--\n\tfreq = " << modulator.oscFreq() << ", amp = " << modulator.oscAmp() << ", sample rate = " <<
             modulator.oscNyquist()*2 << '\n');
	DEBUGLOG("resume() current process level: " << getCurrentProcessLevel() << '\n' << '\n');
	
#endif // DEBUG
	
	AudioEffectX::resume ();
}
//--------------------------------------------------------------------------------------------
void RingModPlug::suspend ()
{
	AudioEffectX::suspend ();
}
//--------------------------------------------------------------------------------------------
RingModPlug::~RingModPlug ()
{
	if (plugPrograms)
		delete[] plugPrograms;
}
//--------------------------------------------------------------------------------------------
bool RingModPlug::getEffectName (char *name)
{
	strcpy(name, "The Ring");
	return true;
}
//--------------------------------------------------------------------------------------------
bool RingModPlug::getProductString (char *text)
{
	strcpy(text, "The Ring");
	return true;
}
//--------------------------------------------------------------------------------------------
bool RingModPlug::getVendorString (char *text)
{
	strcpy(text, "AlgoRhythm Audio");
	return true;
}
//--------------------------------------------------------------------------------------------


#pragma mark ____Plug parameters
// setParameter is only called by the default UI
//--------------------------------------------------------------------------------------------
void RingModPlug::setParameter (VstInt32 index, float value)
{
	RingModProgram &prog = plugPrograms[curProgram];
	
	switch (index) {
		case aAParam_Freq:
			setFreq(value);
			if (curProgram >= aAProg_NPresets)
				prog.prog_paramFreq = value;
			break;
		case aAParam_Depth:
			setDepth(value);
			if (curProgram >= aAProg_NPresets)
				prog.prog_paramDepth = value;
			break;
		case aAParam_Wavetype:
			setWave(prog.prog_paramWavetype = value);
			if (curProgram >= aAProg_NPresets)
				prog.prog_paramWavetype = value;
	}
	
	// need to send parameter value to GUI editor so it will reflect automation from the host
	if (editor)
		((AEffGUIEditor*)editor)->setParameter(index, value);
}
//--------------------------------------------------------------------------------------------
// setParameterAutomated is called by the custom GUI editor
//--------------------------------------------------------------------------------------------
void RingModPlug::setParameterAutomated (VstInt32 index, float value)
{
	RingModProgram &prog = plugPrograms[curProgram];
	
	switch (index) {
		case aAParam_Freq:
			setFreq(value);
			if (curProgram >= aAProg_NPresets)
				prog.prog_paramFreq = value;
			break;
		case aAParam_Depth:
			setDepth(value);
			if (curProgram >= aAProg_NPresets)
				prog.prog_paramDepth = value;
			break;
		case aAParam_Wavetype:
			setWave(prog.prog_paramWavetype = value);
			if (curProgram >= aAProg_NPresets)
				prog.prog_paramWavetype = value;
	}
	
	if (editor)
		((AEffGUIEditor*)editor)->setParameter(index, value);
}
//--------------------------------------------------------------------------------------------
float RingModPlug::getParameter (VstInt32 index)
{
	float param = 0.f;
	
	switch (index) {
		case aAParam_Freq:		param = paramFreq;		break;
		case aAParam_Depth:		param = paramDepth;     break;
		case aAParam_Wavetype:	param = paramWavetype;
	}
	return param;
}
//--------------------------------------------------------------------------------------------
void RingModPlug::getParameterName (VstInt32 index, char *label)
{
	switch (index) {
		case aAParam_Freq:		strcpy(label, "Frequency");		break;
		case aAParam_Depth:		strcpy(label, "Depth");			break;
		case aAParam_Wavetype:	strcpy(label, "Wave Type");
	}
}
//--------------------------------------------------------------------------------------------
void RingModPlug::getParameterLabel (VstInt32 index, char *label)
{
	switch (index) {
		case aAParam_Freq:		strcpy(label, "Hz");	break;
		case aAParam_Depth:		strcpy(label, "%");		break;
		case aAParam_Wavetype:	strcpy(label, "Wave");
	}
}
//--------------------------------------------------------------------------------------------
void RingModPlug::getParameterDisplay (VstInt32 index, char *text)
{
	// handles parameter mapping in the default UI
	switch (index) {
		case aAParam_Freq:
			float2string(float2freq(paramFreq), text, kVstMaxParamStrLen);
			break;
		case aAParam_Depth:
			float2string(float2depth(paramDepth), text, kVstMaxParamStrLen);
			break;
		case aAParam_Wavetype:
			switch (float2wavetype(paramWavetype)) {
				case aAWave_Sine:		strcpy(text, "Sine");			break;
				case aAWave_SawUp:		strcpy(text, "Sawtooth Up");	break;
				case aAWave_SawDn:		strcpy(text, "Sawtooth Down");	break;
				case aAWave_Tri:		strcpy(text, "Triangle");		break;
				case aAWave_Square:		strcpy(text, "Square");
			}
	}
}
//--------------------------------------------------------------------------------------------
bool RingModPlug::canParameterBeAutomated (VstInt32 index)
{
	switch (index) {
		case aAParam_Freq:
			return true;
			break;
		case aAParam_Depth:
			return true;
			break;
		case aAParam_Wavetype:
			return true;
			break;
		default:
			return false;
	}
}
//--------------------------------------------------------------------------------------------


#pragma mark ____Plug programs
//--------------------------------------------------------------------------------------------
void RingModPlug::initFactoryPresets ()
{
	int i;
	for (i = 0; i < aAProg_NPresets; ++i) {
		plugPrograms[i].prog_paramDepth = depth2float(factoryPresets_paramDepth[i]);
		plugPrograms[i].prog_paramFreq = freq2float(factoryPresets_paramFreq[i]);
		plugPrograms[i].prog_paramWavetype = wavetype2float(factoryPresets_paramWavetype[i]);
		strcpy(plugPrograms[i].name, factoryPresets_name[i]);
	}
	for (; i < aAProg_NPrograms; ++i) {
		plugPrograms[i].prog_paramDepth = depth2float(factoryPresets_paramDepth[aAProg_Default]);
		plugPrograms[i].prog_paramFreq = freq2float(factoryPresets_paramFreq[aAProg_Default]);
		plugPrograms[i].prog_paramWavetype = wavetype2float(factoryPresets_paramWavetype[aAProg_Default]);
		strcpy(plugPrograms[i].name, "User");
	}
}
//--------------------------------------------------------------------------------------------
void RingModPlug::setProgram (VstInt32 program)
{
	RingModProgram *prog = &plugPrograms[program];
	curProgram = program;
	
	setDepth(prog->prog_paramDepth);
	setFreq(prog->prog_paramFreq);
	setWave(prog->prog_paramWavetype);
	
	if (editor) {
		// notifying editor of program set prevents knobs from glitching
		((RingModEditor*)editor)->notifyEditorOfProgramSet();
		((AEffGUIEditor*)editor)->setParameter(aAParam_Depth, paramDepth);
		((RingModEditor*)editor)->notifyEditorOfProgramSet();
		((AEffGUIEditor*)editor)->setParameter(aAParam_Freq, paramFreq);
		((AEffGUIEditor*)editor)->setParameter(aAParam_Wavetype, paramWavetype);
	}
}
//--------------------------------------------------------------------------------------------
void RingModPlug::setProgramName (char *name)
{
	strcpy(plugPrograms[curProgram].name, name);
}
//--------------------------------------------------------------------------------------------
void RingModPlug::getProgramName (char *name)
{
	if (!strcmp(plugPrograms[curProgram].name, "Default"))
		sprintf(name, "%s %d", plugPrograms[curProgram].name, curProgram + 1);
	else
		strcpy(name, plugPrograms[curProgram].name);
}
//--------------------------------------------------------------------------------------------
bool RingModPlug::getProgramNameIndexed (VstInt32 category, VstInt32 index, char *text)
{
	if (index < aAProg_NPrograms) {
		strcpy(text, plugPrograms[index].name);
		return true;
	}
	return false;
}
//--------------------------------------------------------------------------------------------


#pragma mark ____Plug processing
//--------------------------------------------------------------------------------------------
void RingModPlug::processReplacing (float **inputs, float **outputs, VstInt32 sampleFrames)
{
	float *inL = inputs[0];
	float *inR = inputs[1];
	float *outL = outputs[0];
	float *outR = outputs[1];
    
	
	// do prep ///////////
	
	int currentWaveType = float2wavetype(paramWavetype);
	if (float2wavetype(lastWave) != currentWaveType) {
		switch (currentWaveType) {
			case tOscType_Sine:     modulator.setType(tOscType_Sine);		break;
            case tOscType_SawUp:	modulator.setType(tOscType_SawUp);		break;
            case tOscType_SawDown:	modulator.setType(tOscType_SawDown);	break;
            case tOscType_Triangle:	modulator.setType(tOscType_Triangle);	break;
            case tOscType_Square:	modulator.setType(tOscType_Square);
		}
		lastWave = paramWavetype;
	}
	
	if (lastFreq != paramFreq) {
		modulator.setFreq(float2freq(paramFreq));
		lastFreq = paramFreq;
	}
	
	modulator.setAmp(paramDepth);
	
    // processing //////////
    
	const float dryDepth = 1.f - paramDepth;
    const int upsampleFrames = sampleFrames * upsampleFactor;
    
    // upsample signal
    interpolator[0].process<float>(inL, tempBuffer[0], sampleFrames);
    interpolator[1].process<float>(inR, tempBuffer[1], sampleFrames);
	
	modulator.genSSE_Linear(modulatorSignal, upsampleFrames);
	
	__m128 *audioSigL = (__m128*)(float*)tempBuffer[0];
	__m128 *audioSigR = (__m128*)(float*)tempBuffer[1];
	__m128 *modSignal = (__m128*)(float*)modulatorSignal;
	__m128 mDryDepth = _mm_set1_ps(dryDepth);
	__m128 mWet, mDry;
	
	for (int i = 0; i < (upsampleFrames>>2); ++i) {
		mWet = _mm_mul_ps(*audioSigL, *modSignal);
		mDry = _mm_mul_ps(*audioSigL, mDryDepth);
		*audioSigL = _mm_add_ps(mWet, mDry);
		mWet = _mm_mul_ps(*audioSigR, *modSignal);
		mDry = _mm_mul_ps(*audioSigR, mDryDepth);
		*audioSigR = _mm_add_ps(mWet, mDry);
		audioSigL++;
		audioSigR++;
		modSignal++;
	}
	
	const uint remainder = upsampleFrames % 4;
	if (remainder) {
		for (uint i = remainder; i > 0; --i) {
			tempBuffer[0][upsampleFrames-i] = (tempBuffer[0][upsampleFrames-i] * modulatorSignal[upsampleFrames-i]);
			tempBuffer[1][upsampleFrames-i] = (tempBuffer[1][upsampleFrames-i] * modulatorSignal[upsampleFrames-i]);
			tempBuffer[0][upsampleFrames-i] += (tempBuffer[0][upsampleFrames-i] * dryDepth);
			tempBuffer[1][upsampleFrames-i] += (tempBuffer[1][upsampleFrames-i] * dryDepth);
		}
	}
	
    // downsample signal back to original sample rate
    decimator[0].process<float>(tempBuffer[0], outL, sampleFrames);
    decimator[1].process<float>(tempBuffer[1], outR, sampleFrames);
	
#if 0	// legacy process function ~~~~~~~~~~
	// process the ring mod on block of audio
	for (int i = 0; i < upsampleFrames; ++i) {
		modulatorVal = modulator.genInterpLinear();
		
		tempBuffer[0][i] = (tempBuffer[0][i] * modulatorVal) + (tempBuffer[0][i] * dryDepth);
		tempBuffer[1][i] = (tempBuffer[1][i] * modulatorVal) + (tempBuffer[1][i] * dryDepth);
	}
#endif
}
//--------------------------------------------------------------------------------------------

