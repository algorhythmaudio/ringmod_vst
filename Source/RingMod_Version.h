//
//
// File:    RingMod_Version.h
// Project: The Ring VST plug-in
//
// Versioning information.
//
//

#ifndef _RingMod_Version_h_
#define _RingMod_Version_h_

#define RingModVersionInt 1
#define RingModVersionStr "Beta v0.9.15"

#endif // _RingMod_Version_h_