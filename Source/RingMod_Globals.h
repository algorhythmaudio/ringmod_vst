//
//
// File:    RingMod_Globals.h
// Project: The Ring VST plug-in
//
// Global macros and constants.
//
//

#ifndef _RingMod_Globals_h_
#define _RingMod_Globals_h_

#include "AADsp.h"


// parameter mapping functions
#define float2freq(x)		( ((x) * freqUpperLimit) + freqLowerLimit )
#define float2depth(x)		( ((x) * 100.f) )
#define float2wavetype(x)	( (int)((x) * (aAWave_NTypes-1)) )
#define freq2float(x)		( ((x) - freqLowerLimit) / freqUpperLimit )
#define depth2float(x)		( ((x) / 100.f) )
#define wavetype2float(x)	( ((float)(x)) / ((float)(aAWave_NTypes-1)) )


// parameter IDs
enum { 
	aAParam_Freq,
	aAParam_Depth,
	aAParam_Wavetype,	// one of wavetype IDs
	aAParam_NParams		// number of parameters
};

// wavetype IDs
enum {
	aAWave_Sine,
	aAWave_SawUp,
	aAWave_SawDn,
	aAWave_Tri,
	aAWave_Square,
	aAWave_NTypes		//  number of wavetypes
};

// program IDs
enum {
    aAProg_Default = 0,
    aAProg_NPresets = 9,
	aAProg_NPrograms = 16
};

// factory preset constant arrays; [0] = default
static const float factoryPresets_paramDepth[aAProg_NPresets] = {
    50.f,
    65.f,
    76.f,
    72.f,
    18.65001f,
    84.4f,
    48.3001f,
    97.55f,
    35.5f
};
static const float factoryPresets_paramFreq[aAProg_NPresets] = {
    2.f,
    575.f,
    42.5f,
    3672.001f,
    3980.f,
    530.f,
    2345.7001f,
    4649.31f,
    1300.f
};
static const float factoryPresets_paramWavetype[aAProg_NPresets] = {
    aAWave_Sine,
    aAWave_Sine,
    aAWave_SawUp,
    aAWave_Tri,
    aAWave_Square,
    aAWave_Square,
    aAWave_SawDn,
    aAWave_SawUp,
    aAWave_Sine
};
static const char* factoryPresets_name[aAProg_NPresets] = {
    "Default",
    "La Campanella",
    "Lumberjack",
    "The Golden Triangle",
    "Buzz Lightyear",
    "Harsh Distortion",
    "Ice Scraper",
    "Bad Circuitry",
    "Halo"
};


// global constants
const float freqUpperLimit		= 5000.f;
const float freqLowerLimit		= 0.f;
const short editorWindowHeight	= 200;
const short editorWindowWidth	= 400;
const ushort wavetableSizeInBits = 11;   // 1 << 11 = 2048 table size
const ushort upsampleFactor		= 6;

#define		freq_UpperLimit_Str     ("5000.00")
#define		freq_LowerLimit_Str		("0.00")
#define		depth_UpperLimit_Str	("100.00")
#define		depth_LowerLimit_Str	("0.00")


#endif	// _RingMod_Globals_h_
