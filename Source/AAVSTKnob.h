//
//
// File:    AAVSTKnob.h
// Project: AlgoRhythm Audio custom VST knob
//
// Created: Dec. 4, 2012 by Christian Floisand
// Version: 1.0
//
// Copyright (c) 2012, AlgoRhythm Audio. All rights reserved.
//
//

#ifndef _AAVSTKnob_h
#define _AAVSTKnob_h

#include "vstgui4/vstgui/plugin-bindings/aeffguieditor.h"


/////////////////////////////////////////////////////////////////////////////
// AAVSTKnob
//
// Custom VST knob that supports smoothing and scalable value change according 
// to the function x^y, where x is the knob's value and y is the curvature.
/////////////////////////////////////////////////////////////////////////////
class AAVSTKnob : public CKnob {
    
public:
    AAVSTKnob (const CRect &size, CControlListener *listener, int32_t tag, CBitmap *background, CBitmap *handle,
               const CPoint &offset=CPoint(0, 0), int32_t drawStyle=kLegacyHandleLineDrawing)
	: CKnob(size, listener, tag, background, handle, offset, drawStyle),
		curvature(1.f),	// 1 = no curvature (linear knob scaling)
		curvature_inv(1.f/curvature)
    {
		// initialize; pi/2 = nyquist = no smoothing
		setSmoothing(M_PI/2.f, 0.);
	}
	
    ~AAVSTKnob () {}
    
    // setCurvature, custom function
    // sets the exponent value (y) of the function x^y that scales the knob's internal value
    // must be > 0
    //-------------------------------------------------------------------------
    void setCurvature (float curv)
    {
		curvature = curv;
        curvature_inv = 1.f / curvature;
	}
	
	// setSmoothing, custom function
	// sets the normalized frequency of the low-pass filter for smoothing parameter change/knob motion
	// must be > 0 <= pi/2; smaller values = more smoothing
	// curValue is set to delay element to avoid glitching in the knob
	//-------------------------------------------------------------------------
	void setSmoothing (float smoothing, float curValue)
	{
		b1 = exp(-2.f * M_PI * smoothing);
		a0 = 1.f - b1;
		z1 = curValue;	// avoid glitching in the knob
	}
	
	// updateDelayElement
	// sets the smoothing filter's delay element to current value to avoid knob glitching
	//-------------------------------------------------------------------------
	void updateDelayElement (float curValue)
	{
		z1 = curValue;
	}
    
    // setValue/getValue
    // fetching the knob's internal value maps the return value to the function x^y
    // setting the knob's internal value reverses the mapping by setting it to x^1/y to maintain the value's linearity internally
    //-------------------------------------------------------------------------
    void setValue (float val)
    {
		CKnob::setValue(powf(val, curvature_inv));
	}
    
    float getValue () const
    {
		if (CKnob::getValue() >= 0.9995)
			return z1 = 1.f;
		else 
			return z1 = a0*powf(CKnob::getValue(), curvature) + b1*z1;
	}
	
	// checkDefaultValue
	// for knob smoothing, we must update the filter's delay element to the current value to avoid glitching
	// when the parameter is set absolutely (i.e. default, program set, text box, etc.)
	//-------------------------------------------------------------------------
	bool checkDefaultValue (CButtonState button)
	{
		if (button.isLeftButton () && button.getModifierState () & kDefaultValueModifier)
		{
			float defValue = getDefaultValue ();
			if (defValue != getValue ())
			{
				// update knob smoothing delay element to avoid glitching
				updateDelayElement(defValue);
				
				// begin of edit parameter
				beginEdit ();
				
				setValue (defValue);
				valueChanged ();
				
				// end of edit parameter
				endEdit ();
			}
			return true;
		}
		return false;
	}
	/*
	CMouseEventResult onMouseDown (CPoint& where, const CButtonState& buttons)
	{
		CKnob::onMouseDown(where, buttons);
	}
	
	CMouseEventResult onMouseUp (CPoint& where, const CButtonState& buttons)
	{
		CKnob::onMouseUp(where, buttons);
	}*/
    
private:
    float curvature, curvature_inv;
	float a0, b1;		// coefficients for simple LP filter
	mutable float z1;	// delay element for LP filter
};

#endif // _AAVSTKnob_h_
