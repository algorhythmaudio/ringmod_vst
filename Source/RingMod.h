//
//
// File:    RingMod.h
// Project: The Ring VST plug-in
//
// Created: June 3, 2012 by Christian Floisand
// Version: Beta v0.9.15
//
// Copyright (c) 2012, AlgoRhythm Audio. All rights reserved.
//
//

#ifndef _RingMod_h_
#define _RingMod_h_

#include "audioeffectx.h"

// RingMod includes
#include "RingMod_Version.h"
#include "RingMod_Globals.h"

// AALibraries
#include "AAOsc.h"
#include "AAArray.h"
#include "AAResample.h"
#ifdef DEBUG
#include "AAPlugDebug.h"
#endif


// program structure for factory presets
struct RingModProgram
{
    RingModProgram () {}
    float prog_paramDepth, prog_paramFreq, prog_paramWavetype;
    char name[kVstMaxProgNameLen];
};

#pragma mark ____RingModPlug Declaration
// -----------------------------------------------------------------------------------------------
class RingModPlug : public AudioEffectX
{
protected:
	float		sampleRate;
    float		paramFreq,			// VST parameters, normalized
                paramDepth,
                paramWavetype;
	float		lastFreq,			// used to test when the freq parameter has changed, so we update it inside processReplacing
                lastWave;			// used to test when the wave parameter has changed, so we update it inside processReplacing
	
	AADsp::AAOsc						modulator;
    AADsp::AAResample::Interpolation    interpolator[2];	// resampling to avoid aliasing
    AADsp::AAResample::Decimation       decimator[2];
	AADsp::AAAlignedArray<float,16>		tempBuffer[2];		// holds upsampled audio signal for processing
	AADsp::AAAlignedArray<float,16>		modulatorSignal;	// modulator's signal used to multiply carrier
    RingModProgram						*plugPrograms;		// factory presets
	VstInt32							maxBlockSize;       // largest block size to be issued by the host

public:
	// initialization and termination
	//----------------------------------------------------------
	RingModPlug (audioMasterCallback audioMaster);
	~RingModPlug ();
	void resume ();
	void suspend ();
	bool getEffectName (char *name);
	bool getVendorString (char *text);
	bool getProductString (char *text);
	VstInt32 getVendorVersion () { return RingModVersionInt; }
	VstPlugCategory getPlugCategory () { return kPlugCategEffect; }
	
	// processing
	//----------------------------------------------------------
	void processReplacing (float **inputs, float **outputs, VstInt32 sampleFrames);
	
	// parameters
	//----------------------------------------------------------
	void setParameter (VstInt32 index, float value);
	void setParameterAutomated (VstInt32 index, float value);
	float getParameter (VstInt32 index);
	void getParameterLabel (VstInt32 index, char *label);
	void getParameterDisplay (VstInt32 index, char *text);
	void getParameterName (VstInt32 index, char *text);
    bool canParameterBeAutomated (VstInt32 index);
	
	// programs
	//----------------------------------------------------------
    void setProgram (VstInt32 program);
    void setProgramName (char *name);
    void getProgramName (char *name);
    bool getProgramNameIndexed (VstInt32 category, VstInt32 index, char *text);
	
    // user-defined
	//----------------------------------------------------------
    void initFactoryPresets ();				// factory presets defined in RingMod_Globals.h
	void setDepth (const float depth)		{ paramDepth = depth; }
	void setFreq (const float freq)			{ paramFreq = freq; }
	void setWave (const float wavetype)		{ paramWavetype = wavetype; }
	
};

	
#endif // _RingMod_h_
